import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReadjustLine {

	static final String keyword1 = "FLAGSTOP";
	static final String keyword2 = "WB";
	static final String keyword3 = "NB";
	static final String keyword4 = "SB";
	static final String keyword5 = "EB";

	public static String changeStopName(String stop_name)
		{
			String stringStopName = stop_name;
			String adjustedStopName = "";
		
				//if it contains only 1 keyword, and its not flagstop - must add one element at the end 
				if((stop_name.contains(keyword2) || stop_name.contains(keyword3) || 
						stop_name.contains(keyword4) || stop_name.contains(keyword5)) && !stop_name.contains(keyword1))
				{
					
					String toChange = stringStopName.substring(0,2);
					adjustedStopName = stringStopName.substring(3).concat(" "+ toChange);
					
					return adjustedStopName;
					
				}
				
				//if it contains only 1 keyword, and its only flagstop - must add one element at the end 
				else if((stop_name.contains(keyword1) && !stop_name.contains(keyword2) && 
						!stop_name.contains(keyword3) && !stop_name.contains(keyword4)) && !stop_name.contains(keyword5))
				{
					
					String toChange = stringStopName.substring(0,2);
					adjustedStopName = stringStopName.substring(3).concat(" "+ toChange);
					
					return adjustedStopName;
				}
				//if it contains 2 keywords, flagstop and 1 of the other 4, must add two elements at the end
				else if (stop_name.contains(keyword1) && stop_name.contains(keyword2))
				{
					
					String toChange = stringStopName.substring(0,11);
					adjustedStopName = stringStopName.substring(12).concat(" "+ toChange);
					
					return adjustedStopName;
					
				}
				else if (stop_name.contains(keyword1) && stop_name.contains(keyword3))
				{
					
					String toChange = stringStopName.substring(0,11);
					adjustedStopName = stringStopName.substring(12).concat(" "+ toChange);
					
					return adjustedStopName;
					
				}
				else if (stop_name.contains(keyword1) && stop_name.contains(keyword4))
				{
					
					String toChange = stringStopName.substring(0,11);
					adjustedStopName = stringStopName.substring(12).concat(" "+ toChange);
					
					return adjustedStopName;
					
				}
				else if (stop_name.contains(keyword1) && stop_name.contains(keyword5))
				{
					
					String toChange = stringStopName.substring(0,11);
					adjustedStopName = stringStopName.substring(12).concat(" "+ toChange);
					
					return adjustedStopName;
					
				}
				else
				{
					return stop_name;
				}
		}
	}
