/*
 * 4. Provide front interface enabling selection between the above features or an option to exit
the programme, and enabling required user input. It does not matter if this is command-line
or graphical UI, as long as functionality/error checking is provided.
You are required to provide error checking and show appropriate messages in the case of erroneous
inputs � eg bus stop doesn�t exist, wrong format for time for bus stop (eg letters instead of
numbers), no route possible etc. 
 */

import javax.swing.*;  
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FrontInterface {

	public static void main (String[] args) throws FileNotFoundException
	{
		System.out.println("Welcome to the Vancouver Bus System. Please choose an option to proceed. ");
		System.out.println("\nEnter A if you would like for the system to search for the shortest path between 2 bus stops. ");
		System.out.println("Enter B if you would like for the system to search for a specific bus stop. ");
		System.out.println("Enter C if you would like for the system to search all trips in a given arrival time.");
		System.out.println("Enter D if you do not want to continue. ");
		
		Scanner input = new Scanner (System.in);
		
			if (input.hasNext())
			{
				if(input.hasNext("A")||input.hasNext("a"))
				{
					System.out.println("Unable to calculate at this time, rerun and choose another option");
				}
				else if(input.hasNext("B")||input.hasNext("b"))
				{
					boolean ready = false;
					while(!ready)
					{
						System.out.println("Enter 1 if you want to Search for a specific stop by typing the full address (FLAGSTOP/WB/NB/SB/EB AT END)"
								+ "\nor 2 if you want to search by the first full word in the address: \n");
						Scanner scanner = new Scanner(System.in);
						int optionTaken = scanner.nextInt();
					
						if (optionTaken == 1 || optionTaken == 2)
						{
							TST.BusStopTST(optionTaken);
							ready = true;
						}
						else 
						{
							System.out.print("Bus Stop Invalid, press your option again.");
							
						}
					}
				}
				else if(input.hasNext("C")||input.hasNext("c"))
				{
					System.out.println("This may take a moment");
					
					boolean finished = false;
					while (!finished) 
					{
						ArrayList<String> validArrivalStops = new ArrayList<>();
						ArrayList<String> stopsData = new ArrayList<>();
						
						arrivalTimesCriteria.reconfigureFile(validArrivalStops);
						
						System.out.println("Please enter an arrival time in the format hh:mm:ss to find all possible trips: ");
						Scanner scanner1 = new Scanner(System.in);
						String userInput = scanner1.next();
						
						boolean cont = arrivalTimesCriteria.isValid(userInput);
						
						//for loop to find matches between arrival times and inputs
						if(cont) 
						{
							int matches = 0;
							for(int i = 0; i < validArrivalStops.size(); i++)
							{
								String tripInfo = validArrivalStops.get(i);
								String [] splitTripInfo = tripInfo.split(",");
								
								if(userInput.trim().equals(splitTripInfo[1].trim()))
								{
									stopsData.add(tripInfo);
									matches = matches + 1;
								}
							}
							if(matches == 0)
							{
								System.out.println("No matches found for your input. ");
							}
							
							arrivalTimesCriteria.showIdsInOrder(stopsData);
							finished = true;
						}
						else
						{
								System.out.println("Invalid Time inputted, please try again ");
						}	
					}
				}	
				else if(input.hasNext("D")||input.hasNext("d"))
				{
					System.out.println("Goodbye");
				}
				else
				{
					System.out.println("Invalid input recognised. Please enter A, B, C or D.");
				}
		}	
	}
}