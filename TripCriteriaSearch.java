/*Searching for all trips with a given arrival time, returning full details of all trips matching the
criteria (zero, one or more), sorted by trip id
Arrival time should be provided by the user as hh:mm:ss. When reading in stop_times.txt file you
will need to remove all invalid times, e.g., there are times in the file that start at 27/28 hours, so are
clearly invalid. Maximum time allowed is 23:59:59.
*/



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class arrivalTimesCriteria {
	
	public static final int stop_times_file_max_parameters = 8;
	public static final int max_hours = 23;
	public static final int min_hours = 0;
	public static final int max_mins = 59;
	public static final int min_mins = 0;
	public static final int max_seconds = 59;
	public static final int min_seconds = 0;

	//remake file so that invalid times are not included with comparison to userInput
	public static void reconfigureFile( ArrayList<String> validTimes) throws FileNotFoundException
	{
		File stop_times_file = new File("stop_times.txt");
		Scanner scanner = new Scanner(new FileReader(stop_times_file));
		scanner.nextLine(); //gets rid of header
		
		while(scanner.hasNextLine())
		{
			String row = scanner.nextLine();
			String[] stop_data = row.trim().split(","); //splits the line from stop_times to isolate arrival time for comparison to input
			String arrival_time = stop_data[1];
			
			if(isValid(arrival_time))
			{
				validTimes.add(row);
			}
		}
	}
	
	public static boolean isValid(String arrival_time) //double check the right syntax was entered
	{
		boolean valid;
		String[] split_arrival_time = arrival_time.split(":");
		
		int hours = Integer.parseInt(split_arrival_time[0].trim()); //don't want spaces to 
		int mins = Integer.parseInt(split_arrival_time[1].trim());
		int seconds = Integer.parseInt(split_arrival_time[2].trim());
		
		if (hours <= max_hours && mins <= max_mins && seconds <= max_seconds &&
				hours >= min_hours && min_mins >= 0 && seconds >= min_seconds)
		{
			valid = true;
		}
		else 
		{
			valid = false;
		}
		return valid;
	}
	
	//want to have the rows of the matching arrivals - get all data of specific matching rows
	public static void showIdsInOrder(ArrayList<String> stopsData)
	{
		String [][] validArrivalTimes = new String[stopsData.size()][stop_times_file_max_parameters];
		
		for(int i = 0; i < stopsData.size();i++)
		{
			String fullTripData = stopsData.get(i);
			String[] splitTripData = fullTripData.split(",");
			
			for(int j = 0; j < validArrivalTimes[i].length; j++)
			{
				validArrivalTimes[i][j] = splitTripData[j];
			}
		}
		//bubble sort matched arrival times to give sorted list
		String [][] sortedTrips = bubbleSort(validArrivalTimes);
		
		if (sortedTrips == null)
		{
			System.out.println(" ");
		}
		else 
		{
			System.out.println("\nList of available trips:");
			
			for(int i = 0; i < sortedTrips.length; i++)
			{
				System.out.println("\nTrip ID:" + sortedTrips[i][0]+"\nArrival Time:" + 
				sortedTrips[i][1]+"\nDeparture Time:" + sortedTrips[i][2]+"\nStop ID:" + 
				sortedTrips[i][3]+"\nStop Sequence:" + sortedTrips[i][4]+
				"\nStop Headsign:" + sortedTrips[i][5]+"\nPickup Type:" + 
				sortedTrips[i][6]+"\nDropoff Type:" + sortedTrips[i][7]);
			}
		}
	}
	
	public static String[][] bubbleSort(String[][] validArrivalTimes)
	{
		String[][] sortedArrivalTimes;
		
		boolean done = false;
		String[] temporaryArray;
		
		while(!done)
		{
			done = true;
			for(int i = 0; i < validArrivalTimes.length - 1; i++)
			{
				int ID = Integer.parseInt(validArrivalTimes[i][0]);
				int compareID = Integer.parseInt(validArrivalTimes[i+1][0]);
				
				if (ID>compareID)
				{
					temporaryArray = validArrivalTimes[i];
					validArrivalTimes[i] = validArrivalTimes[i+1];
					validArrivalTimes[i+1] = temporaryArray;
					done = false;
				}
			}
		}
		return validArrivalTimes;
	}
}
